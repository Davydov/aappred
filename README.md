# About
AAPPred epitope prediction software based on the algorithm described in the article [Prediction of linear B-cell epitopes using amino acid pair antigenicity scale](http://www.ncbi.nlm.nih.gov/pubmed/17252308).

We have implemented two modes: first (SVM1) one is based on amino acids pair (AAP) frequencies and classical amino acids scales such as hydrophilicity, flexibility, etc. SVM2 uses only AAP frequencies to predict epitopes.

Our data set contains not only publicly available data but also our proprietary experimental data.

You can find an online version here: [AAPPred](http://bioinf.ru/aappred/).

Please cite us: Ya.I. Davydov, A.G. Tonevitsky, 2009, published in
[Molekulyarnaya Biologiya, 2009, Vol. 43, No. 1, pp. 166–174](https://dx.doi.org/10.1134/S0026893309010208).
